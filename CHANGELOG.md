## 8.0.0

* Updated RuStore billing SDK to 8.0.0
* `available` method returns the `PurchaseAvailabilityResult` class
* `available` method is marked as deprecated
* Added getAuthorizationStatus method

## 7.0.1

* Add support gradle 8
* Fix purchaseInfo func

## 7.0.0

* Updated RuStore billing SDK to 7.0.0
* Removed conflicting dependencies

## 6.1.0

* Updated RuStore billing SDK to 6.1.0

## 6.0.2

* Fixed sandbox crash
* Fixed native error handling

## 6.0.1

* Updated RuStore billing SDK to 6.0.0
* Fix dynamic version

## 6.0.0

* Upgrade SDK version to 6.0.0
* Deleted field desciption from Purchase
* Added field productType to Purchase

## 5.0.2

* Fix crash while rustore unauthorized

## 5.0.1

* Fixes with build and error handling

## 5.0.0

* Internal fixes
* Updated version to 5.0.0

## 4.0.0

* Updated RuStore billing SDK to 5.0.0
* Addded new method purchaseInfo
* Added onNewIntent logic for deeplinks
* Added native RuStore billing SDK error-handling  

## 3.1.0

* Updated RuStore billing SDK to 3.1.0.
* Added debug params
* Updated Pigeon to ^14.0.0

## 3.0.0

* Updated RuStore billing SDK to 3.0.0.

## 2.0.0

* Updated RuStore billing SDK to 2.2.0.

## 1.0.0

* Updated RuStore billing SDK to 1.1.1.

## 0.0.5

* Fixed error on using with review library.

## 0.0.4

* Updated RuStore billing SDK to 0.1.8.

## 0.0.3

* Added internal analytics

## 0.0.2

* Updated RuStore billing SDK to 0.1.6.

## 0.0.1

* Full RuStore billing SDK support.
